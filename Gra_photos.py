#!/usr/bin/env python
# coding=utf-8
import urllib
# import urllib2
import re

def getHtml(url):
    page = urllib.urlopen(url)
    html = page.read()
    return html

def download_file(download_url, file_name):
	try:
		urllib.urlretrieve(download_url, file_name) 
	except:
		print 'Error retrieving the URL:', download_url
	# response = urllib2.urlopen(download_url)
	# with open(file_name, 'w') as file:
	# 	file.write(data)

if __name__ == '__main__':
	save_path = '/Users/Smellly/Desktop/Graduation_photos/'
	page = 0
	url_list = []

	p1 = re.compile(r'File3Show/20160620_\d{6}\(?\d*\)?\.jpg')

	print 'Create url_list'
	while page < 148:
		url = 'http://xscp.xmu.edu.cn/Default.aspx?page='\
		+str(page)+\
		'&fileShow=File3Show&fileDown=File3Down'
		url_list.append(url)
		page += 1

	for url in url_list:
		print url
		html = getHtml(url)
		# http://xscp.xmu.edu.cn/File3Show/20160620_094644.jpg
		download_list = []
		# print html
		l = set(p1.findall(html))

		for i in l:
			#print i
			download_url = 'http://xscp.xmu.edu.cn/' + i
			file_name = save_path + i.split('/')[-1]
			# print 'file_name:', file_name
			# print 'url:', download_url
			download_file(download_url, file_name)



	
